(##namespace ("chandai#short-note#"))
(##include "~~lib/gambit#.scm")

(##include "chandai#.scm")
(##include "git#.scm")
(##include "grep#.scm")
(##include "misc#.scm")
(##include "native#.scm")
(##include "time#.scm")

(define (make-short-note-hook prefix path time note) #t)

(define (make-short-note prefix note)
  (define (prepare-path path)
    (or (file-exists? path)
	(create-directory-recursively (path-strip-trailing-directory-separator (path-directory path)))))

  (git-init)
  (let* ((time (make-current-time))
	 (path (time->short-note-path prefix time)))
    (if (make-short-note-hook prefix path time note)
	(begin
	  (prepare-path (path-expand path))
	  (with-output-to-file
	      (list (string->keyword "path") path (string->keyword "append") #t)
	    (lambda ()
	      (print (time->note-string time))
	      (print " ")
	      (println (if (match-curly-at-tag note 0)
			   (substitute-at-tags note
					       (reverse (map c-sanitize-note-for-linking
							     (with-input-from-file
								 (string-append chandai-root "/" prefix "/.recent")
							       read))))
			   note))))
	  (invalidate-recent-cache prefix)))))

; Apply time filter, then grep through "notes/default"
; Return a list of matched lines without duplicates removed

; Format could be
; grep pattern -- path
(define (grep-short-notes prefix patterns pathspecs)
  (let* ((full-prefix (string-append chandai-root "/" prefix))
	 (files (ls-files pathspecs full-prefix)))
    (grep-files (make-grep-sexp-fn grep-re-fn patterns) files)))

(define (list-short-notes prefix patterns pathspecs)
  (let ((result (grep-short-notes prefix patterns pathspecs)))
    (cache-search-results prefix result)
    (map (lambda (x) (c-println x)) result)))

(define (list-open-short-todos prefix patterns pathspecs)
  (define (open-todo? x)
    (define (has-end? list)
      (cond ((pair? list)
	     (if (c-strstr (car list) "#end")
		 #t
		 (has-end? (cdr list))))
	    (null?
	     #f)
	    (else
	     (c-strstr list "#end"))))

    (let* ((todo-tag (short-note->todo-search-tag x))
	   (todo (grep-short-notes prefix (list 'substr-fn todo-tag) pathspecs)))
      (not (has-end? todo))))

  (define (filter list pred?)
    (cond ((pair? list)
	   (if (pred? (car list))
	       (cons (car list) (filter (cdr list) pred?))
	       (filter (cdr list) pred?)))
	  ((null? list)
	   '())
	  (else
	   (if (pred? list)
	       (cons list)
	       '()))))

  (let* ((result (grep-short-notes prefix patterns pathspecs))
	 (new-result (filter result open-todo?)))
    (cache-search-results prefix new-result)
    (for-each (lambda (x) (c-println x)) new-result)))

(define (cache-search-results prefix results)
  (with-output-to-file (string-append chandai-root "/" prefix "/.recent")
    (lambda () (pretty-print results))))

(define (invalidate-recent-cache prefix)
  (let ((path (string-append chandai-root "/" prefix "/.recent")))
    (and (file-exists? path) (eq? (file-type path) 'regular) (delete-file path))))

(define (recall-short-note prefix index)
  (list-ref
   (reverse (with-input-from-file (string-append chandai-root "/" prefix "/.recent") read))
   (- index 1)))

(define (list->string list)
  (string-append (car list)
		 (apply string-append
			(map
			 (lambda (x)
			   (string-append " " x))
			 (cdr list)))))

(define (recall-short-todo prefix index args)
  (make-short-note prefix
		   (string-append (list->string args)
				  " "
				  (short-note->todo-tag
				   (recall-short-note prefix (- index))))))

(define (update-short-note prefix old-note new-note)
  (define (write-out contents)
    (let loop ((first-line contents)
	       (result #f))
      (if (null? first-line)
	  result
	  (if (string=? old-note (car first-line))
	      (begin
		(println new-note)
		(loop (cdr first-line) #t))
	      (begin
		(println (car first-line))
		(loop (cdr first-line) result))))))

  (if (same-short-note? new-note old-note)
      (let ((path (short-note->path prefix old-note)))
	(current-directory chandai-root)
	;; check for git uptodate
	(let ((contents (call-with-input-file path (lambda (x) (read-all x read-line)))))
	  (if (with-output-to-file (list (string->keyword "path") path (string->keyword "truncate") #t)
		(lambda () (write-out contents)))
	      (invalidate-recent-cache prefix)
	      (println "Notes does not match"))))
      (println "Notes does not match")))

(define (short-note->text note)
  ;; date-time takes 24 letters
  (substring note 25 (string-length note)))

(define (short-note->todo-tag note)
  (string-append "@todo[" (c-sanitize-note-for-linking note) "]"))

(define (short-note->todo-search-tag note)
  (string-append "@todo[" (substring note 0 24)))

(define (short-note->path prefix note)
  (time->short-note-path prefix (note-string->time note)))

(define (same-short-note? note1 note2)
  (string=? (note-string->time note1) (note-string->time note2)))

;; #clock handling

(define (list->time-records lst)
  (if (pair? lst)
      ;; ignore redundant #end
      (if (c-strstr (car lst) "#end")
	  (list->time-records (cdr lst))
	  (if (pair? (cdr lst))
	      (let ((result (cons (car lst) (cadr lst))))
		(cons result (list->time-records
			      (if (c-strstr (cdr result) "#end")
				  (cddr lst)
				  (cdr lst)))))
	      (cons (cons (car lst) #f) '())))
      '()))

;; It's like list->time-records but with tail recursion
(define (list->last-time-record lst)
  (if (pair? lst)
      ;; ignore redundant #end
      (if (c-strstr (car lst) "#end")
	  (list->last-time-record (cdr lst))
	  (if (pair? (cdr lst))
	      (let ((result (cons (car lst) (cadr lst))))
		(list->last-time-record
		 (if (c-strstr (cdr result) "#end")
		     (cddr lst)
		     (cdr lst))))
	      (car lst)))
      #f))

(define (list-time-records prefix patterns pathspecs)
  (let* ((lines (grep-short-notes prefix
				  (cond
				   ((string? patterns)
				    (list 'and "#clock" patterns))
				   ((pair? patterns)
				    (list 'and "#clock" patterns))
				   (else
				    "#clock"))
				  pathspecs))
	 (time-recs (list-uniq (list->time-records lines)
			       (lambda (op1 op2)
				 (let* ((op1 (car op1))
					(op1-len (string-length op1))
					(tail2 (cdr op2))
					(op2 (car op2))
					(op2-len (string-length op2)))
				   (and tail2 ; if tail2 is #f, it's open record, do not match
					(= op1-len op2-len)
					(string=? (substring op1 25 op1-len)
						  (substring op2 25 op2-len))))))))
    (cache-search-results prefix
			  (map (lambda (x)
				 (car x))
			       time-recs))
    (map (lambda (x)
	   (c-println (if (eq? (cdr x) #f)
			  (string-append (car x) " (ongoing)")
			  (car x))))
	 time-recs)))

(define (list-last-time-record prefix patterns pathspecs)
  (let* ((lines (grep-short-notes prefix
				  (cond
				   ((string? patterns)
				    (list 'and "#clock" patterns))
				   ((pair? patterns)
				    (list 'and "#clock" patterns))
				   (else
				    "#clock"))
				  pathspecs))
	 (last-time-rec (list->last-time-record lines)))
    (and last-time-rec
	 (c-println last-time-rec))))

(define (make-time-record prefix str)
  (let ((note (if (c-strstr str "#clock")
		  str
		  (string-append str " #clock"))))
    (make-short-note prefix note)
    (c-println note)))

; Should check if last record is closed already
(define (close-last-record prefix)
  (make-short-note prefix "#end #clock"))

(define (recall-time-record prefix index)
  (let ((text (short-note->text (recall-short-note prefix (- index)))))
    (make-time-record prefix text)))
