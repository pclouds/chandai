(##namespace ("chandai#utils#"))
(##include "~~lib/gambit#.scm")

(define (string-match-fsm s start match? fun data)
  (let* ((length (string-length s))
	 (match-1 (lambda (start match? fun data) ; it's basically a finite state machine
		    (let next ((start start)
			       (match? match?)
			       (fun fun)
			       (data data))
		      (call/cc
		       (lambda (exit)
			 (cond
			  ((>= start length)
			   (exit #f))
			  ((match? start data)
			   (let ((result (fun start data)))
			     (case (car result)
			       ((done)
				(exit (cdr result)))
			       ((continue)
				(apply next (cdr result)))
			       (else
				(raise "Unexpected symbol")))))
			  (else
			   (next (+ start 1) match? fun data)))))))))
    (match-1 start match? fun data)))

;; This is FSM for @blah(something) where ( and ) are open-bracket
;; and closing-bracket respectively.
(define-macro (make-match-at-tag name open-bracket closing-bracket)
  `(define (,name s start)
     (string-match-fsm s
		       start
		       (lambda (start data)
			 (eq? (string-ref s start) #\@))
		       (lambda (start data)
			 (list 'continue
			       (+ start 1)
			       (lambda (start data)
				 (eq? (string-ref s start) ,open-bracket))
			       (lambda (start data)
				 (list 'continue
				       (+ start 1)
				       (lambda (start data)
					 (eq? (string-ref s start) ,closing-bracket))
				       (lambda (start data)
					 (list 'done (car data) start))
				       (cons start data)))
			       data))
		       '())))

(make-match-at-tag match-at-tag #\[ #\]) ; (define (match-at-tag s start) ...)
(make-match-at-tag match-curly-at-tag #\{ #\}) ; (define (match-curly-at-tag s start) ...)

(define (substitute-at-tags s recall-list)
  (define (substitute-at-tags-1 s)
    (define (substitute result)
      (let* ((first (substring s 0 (car result)))
	     (old-middle (substring s (+ (car result) 1) (cadr result)))
	     (new-middle (list-ref recall-list
				   (- (- (string->number old-middle)) 1)))
	     (last (substring s (+ (cadr result) 1) (string-length s))))
	(string-append first "[" new-middle "]" (substitute-at-tags-1 last))))

    (cond
     ((match-curly-at-tag s 0) => substitute)
     (else s)))

  (substitute-at-tags-1 s))

(define (list-uniq lst pred?)
  (cond
   ((pair? lst)
    (cons (car lst)
	  (list-uniq (call/cc
		      (lambda (exit)
			(let loop ((lst-2 (cdr lst)))
			  (cond
			   ((pair? lst-2)
			    (if (pred? (car lst) (car lst-2))
				(loop (cdr lst-2))
				(exit lst-2)))
			  (else
			   (exit lst-2))))))
		     pred?)))
   (else
    lst)))

(define (create-directory-recursively path)
  (or (file-exists? path)
      (let ((parent-path (path-strip-trailing-directory-separator (path-directory path))))
	(create-directory-recursively parent-path)
	(create-directory path))))
