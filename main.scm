(##include "chandai#.scm")
(##include "long-note#.scm")
(##include "misc#.scm")
(##include "native#.scm")
(##include "short-note#.scm")
(##include "time#.scm")

(define (main command args)
  ;; Basically I need a match and dispatch function, how?
  (define (parseopt spec args)
    (define (run new-spec)
      (parseopt spec ((cdr new-spec) args)))

    ;; "cond" form with else clause to call (run 'default)
    (define-macro (cond* lst . body)
      `(cond
	 ,lst ,@body
	 (else
	   (cond
	     ((assoc 'default spec) => run)
	     (else
	       args)))))

    (cond*
     ((pair? args)
      (cond*
       ((assoc (car args) spec) => run)
       ((char=? (string-ref (car args) 0) #\-)
	(cond*
	 ((assoc 'all-options spec) => run)))))
     ((or (eq? args #f) (eq? args #!void))
      #f)))

  (define (parse-pathspecs args)
    (let ((result (member "--" args)))
      (if result
	  (cdr result)
	  '())))

  (define (parse-patterns args)
    (cond
     ((pair? args)
      (let loop ((args args))
	(if (pair? args)
	    (if (not (string=? (car args) "--"))
		(cons (car args) (loop (cdr args)))
		'())
	    '())))
     (else
      #f)))

  (define (append-tag tag args)
    (let ((patterns (parse-patterns args)))
      (cond
       ((pair? patterns)
	(list 'and tag patterns))
       (else
	tag))))

  (define (recall prefix fn)
    (lambda (args)
      (fn prefix (string->number (car args)) (cdr args))))

  (let ((prefix "notes/default")
	(long-prefix "memos/default"))
    (case command
      ((no)
       (make-short-note prefix (list->string args)))

      ((lno)
       (parseopt (list (cons "-all"
			     (lambda (args)
			       (list-short-notes prefix
						 (parse-patterns (cdr args))
						 (parse-pathspecs (cdr args)))
			       #f))
		       (cons 'all-options
			     (lambda (args)
			       (let* ((recent-list (reverse (with-input-from-file
								(string-append chandai-root "/" prefix "/.recent")
							      read)))
				      (sanitized-recent-list (map c-sanitize-note-for-linking recent-list))
				      (number-str (cond
						   ((c-strstr (car args) "@") => (lambda (x)
										   (substring (car args) 0 x)))
						   (else
						    (car args))))
				      (id (let ((ref (- (- (string->number number-str)) 1)))
					    (cond
					     ((c-strstr (car args) "@") => (lambda (x)
									     (let* ((str (car args))
										    (len (string-length str))
										    (ref (list-ref recent-list ref))
										    (ref-len (string-length ref))
										    (tag (string-append (substring str x len) "["))
										    (pos (+ (c-strstr ref tag) (string-length tag))))
									       (substring ref pos (+ pos 24)))))
					     (else
						(substring (list-ref sanitized-recent-list ref) 0 24)))))
				      (match-id (lambda (line)
						  (string=? (substring line 0 24) id)))
				      (match-tag (lambda (line)
						   (cond
						    ((match-at-tag line 25) => (lambda (result)
										 (let* ((start (+ (car result) 1))
											(end (+ start 24)))
										   (and (>= (- (cadr result) start) 24)
											(string=? (substring line
													     start
													     end)
												  id)))))
						    (else
						     #f)))))
				 (list-short-notes prefix
						   (list match-id ; default logic is or
							 (cons 'and
							       (cons match-tag
								     (cdr args))))
						   (parse-pathspecs (cdr args))))
			       #f))
		       (cons 'default
			     (lambda (args)
			       (list-short-notes prefix
						 (let ((patterns (parse-patterns args)))
						   (cond
						    ((pair? patterns)
						     (list 'and (list 'not (list "#clock" "#end" "#todo")) patterns))
						    (else
						     (list 'not (list "#clock" "#end" "#todo")))))
						 (parse-pathspecs args))
			       #f)))
		 args))

      ((cx)
       (close-last-record prefix))

      ((c)
       (parseopt (list (cons 'all-options
			     (lambda (args)
			       (recall-time-record prefix
						   (string->number (car args)))
			       #f))
		       (cons 'default
			     (lambda (args)
			       (make-time-record prefix
						 (list->string args))
			       #f)))
		 args))

      ((lc)
       (list-time-records prefix (parse-patterns args) (parse-pathspecs args)))

      ((lwc)
       (list-last-time-record prefix (parse-patterns args) (parse-pathspecs args)))

      ((t)
       (parseopt (list (cons 'all-options
			     (lambda (args)
			       (recall-short-todo prefix
						  (string->number (car args))
						  (cdr args))
			       #f))
		       (cons 'default
			     (lambda (args)
			       (make-short-note prefix
						(string-append (list->string args) " #todo"))
			       #f)))
		 args))

      ((lt)
       (parseopt (list (cons "-all"
			     (lambda (args)
			       (list-short-notes prefix
						 (append-tag "#todo" (cdr args))
						 (parse-pathspecs (cdr args)))
			       #f))
		       (cons 'default
			     (lambda (args)
			       (list-open-short-todos prefix
						      (append-tag "#todo" args)
						      (parse-pathspecs args))
			       #f)))
		  args))

      ((mknote)
       (make-long-note long-prefix (list->string args)))

      ((lsnote)
       (list-long-notes long-prefix (parse-patterns args) (parse-pathspecs args)))

      ((donote)
       (parseopt (list (cons 'all-options
			     (lambda (args)
			       (let* ((note (recall-short-note long-prefix (- (string->number (car args)))))
				      (id (substring note 0 24))
				      (tm (note-string->time id))
				      (path (time->long-note-path long-prefix tm)))
				 (current-directory path)
				 (shell-command (getenv "SHELL"))
				 #f))))
		 args))

      (else (print "Unknown\n")))))

(or (member (car (command-line)) (list "gsi"))
    (begin
      (let ((hook (string-append chandai-root "/hooks.scm")))
	(and (file-exists? hook)
	     (load hook)))
      (main (string->symbol (path-strip-directory (car (command-line)))) (cdr (command-line)))))
