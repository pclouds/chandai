SCM_FILES = \
	misc.scm \
	chandai.scm \
	git.scm \
	grep.scm \
	long-note.scm \
	native.scm \
	short-note.scm \
	sort.scm \
	srfi-19.scm \
	time.scm \
	main.scm

SSCM_FILES =

ifdef SINGLE_HOST
C_FILES = build.c
OBJ_FILES = $(SSCM_FILES:.scm=.o) build.o m_.o
else
C_FILES = $(SCM_FILES:.scm=.c)
OBJ_FILES = $(SSCM_FILES:.scm=.o) $(SCM_FILES:.scm=.o) m_.o
endif
GSC = gsc-script
GSC_FLAGS = -debug-source

all: m

build.scm: $(SCM_FILES)
	test -f $@ && rm $@; for i in $(SCM_FILES); do echo "(##include \"$$i\")" >> $@;done

build.c: build.scm
	gsc-script -debug-source -c $@ $^

$(patsubst %.scm,%.c,$(SSCM_FILES)): %.c: %.scm
	$(GSC) -:s $(GSC_FLAGS) -c $@ $^

$(patsubst %.scm,%.c,$(SCM_FILES)): %.c: %.scm
	$(GSC) $(GSC_FLAGS) -c $@ $^

$(patsubst %.scm,%.o1,$(SSCM_FILES)): %.o1: %.scm
	if test -f $@; then rm $@; fi
	$(GSC) -:s $(GSC_FLAGS) $^

$(patsubst %.scm,%.o1,$(SCM_FILES)): %.o1: %.scm
	if test -f $@; then rm $@; fi
	$(GSC) $(GSC_FLAGS) $^

# Source order here is important
m_.c: $(C_FILES)
	$(GSC) -link -o $@ $^

m: $(OBJ_FILES)
	$(CC) -o $@ $^ -lgambc

clean:
	rm m *.c *.o *.o1 build.scm
