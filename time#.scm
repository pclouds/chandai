(##namespace
 ("chandai#time#"
  make-current-time
  note-string->time
  time->long-note-path
  time->note-string
  time->short-note-path
  ))
