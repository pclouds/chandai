(##namespace ("chandai#time#"))
(##include "~~lib/gambit#.scm")

(##include "chandai#.scm")
(##include "native#.scm")
(##include "srfi-19#.scm")

(define (make-current-time) (current-date (c-timezone)))

(define (time->short-note-path prefix time)
  (define (time->path time)
    (let ((utc-time (time-utc->date (date->time-utc time) 0)))
      (date->string utc-time "~Y/~m/~d")))
  (string-append chandai-root "/" prefix "/" (time->path time)))

(define (time->long-note-path prefix time)
  (define (time->path time)
    (let ((utc-time (time-utc->date (date->time-utc time) 0)))
      (date->string utc-time "~Y/~m/~d-~H-~M-~S")))
  (string-append chandai-root "/" prefix "/" (time->path time)))

;; FIXME DST involved
(define (time->note-string time)
  (string-append (date->string time "~Y/~m/~d ~H:~M:~S~z")))

(define (note-string->time note)
  (let ((time-string (substring note 0 24)))
    (string->date time-string "~Y/~m/~d ~H:~M:~S~z")))
