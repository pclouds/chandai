(load "root")

(test-begin "chandai root setup")

(test-success "root setup"
  (begin
    (with-root "gone" (lambda () #t))
    (not (file-exists? "gone"))))

(test-end)
