(define-macro (test-begin description)
  (let* ((has-var? (lambda (var)
		     (with-exception-handler
		      (lambda (exc)
			#f)
		      (lambda () (string=? (getenv var) "1")))))
	 (verbose? (has-var? "V")))
    `(begin
       (define test-description ,description)
       (define test-counter 0)
       (define fail-counter 0)
       (define (test-passed title fn)
	 (set! test-counter (+ test-counter 1)))
       (define (test-failed title fn)
	 (set! test-counter (+ test-counter 1))
	 (set! fail-counter (+ fail-counter 1)))
       (define test-verbose? ,verbose?)
       ,(if verbose?
	`(define print-verbose print)
	`(define (print-verbose . body) #t)))))

(define-macro (test-end)
  `(begin
     (print "** " test-description ": "
	    test-counter " tests, "
	    fail-counter " failed\n")
     (exit)))

(define-macro (test-success title . fn)
  `(begin
     ,@(map (lambda (fn)
	     `(if ,fn
		  (begin
		    (print-verbose "* " ,title "\n")
		    (test-passed ,title ',fn))
		  (begin
		    (print-verbose " FAILED " ,title "\n")
		    (and test-verbose? (pp ',fn))
		    (print-verbose "\n")
		    (test-failed ,title ',fn))))
	   fn)))
