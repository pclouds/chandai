(load "../native")

(##include "../native#.scm")

(test-begin "native.scm")

(test-success "c-strstr"
  (equal? (c-strstr "abc" "a") 0)
  (equal? (c-strstr "abc" "b") 1)
  (equal? (c-strstr "abc" "d") #f))

(test-end)
