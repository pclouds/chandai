(load "../misc")

(##include "../misc#.scm")

(test-begin "misc.scm")

(test-success "match-at-tag"
  (not (match-at-tag "abc" 0))
  (not (match-at-tag "@abc" 0))
  (not (match-at-tag "@abc[" 0))
  (equal? (match-at-tag "@abc[]" 0) (list 4 5))
  (equal? (match-at-tag "@abc[foo]" 0) (list 4 8)))

(test-success "match-curly-at-tag"
  (not (match-curly-at-tag "abc" 0))
  (not (match-curly-at-tag "@abc" 0))
  (not (match-curly-at-tag "@abc{" 0))
  (equal? (match-curly-at-tag "@abc{}" 0) (list 4 5))
  (equal? (match-curly-at-tag "@abc{foo}" 0) (list 4 8)))

(test-success "substitute-at-tags"
  (equal? (substitute-at-tags "@foo" (list "one" "two" "three")) "@foo")
  (equal? (substitute-at-tags "@foo{-1}" (list "one" "two" "three")) "@foo[one]")
  (equal? (substitute-at-tags "@foo{-2}" (list "one" "two" "three")) "@foo[two]")
  (equal? (substitute-at-tags "@foo{-3}" (list "one" "two" "three")) "@foo[three]")
  (equal? (substitute-at-tags "@foo{-3} @bar{-1}" (list "one" "two" "three")) "@foo[three] @bar[one]"))

(test-success "list-uniq"
  (equal? (list-uniq '() =) '())
  (equal? (list-uniq '(1) =) '(1))
  (equal? (list-uniq '(1 1) =) '(1))
  (equal? (list-uniq '(1 1 1 1) =) '(1))
  (equal? (list-uniq '(1 2 2 2 3) =) '(1 2 3)))

(test-end)
