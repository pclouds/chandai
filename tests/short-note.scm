(load "../git")
(load "../sort")
(load "../grep")
(load "root")

(##include "../short-note#.scm")
(##include "../chandai#.scm")

(test-begin "short note tests")

(test-success "list->time-records"
  (equal? (list->time-records '()) '())
  (equal? (list->time-records '("#end")) '())
  (equal? (list->time-records '("a")) '(("a" . #f)))
  (equal? (list->time-records '("a" "#end")) '(("a" . "#end")))
  (equal? (list->time-records '("a" "#end" "#end")) '(("a" . "#end")))
  (equal? (list->time-records '("a" "b")) '(("a" . "b") ("b" . #f)))
  (equal? (list->time-records '("a" "b" "c")) '(("a" . "b") ("b" . "c") ("c" . #f)))
  (equal? (list->time-records '("a" "b" "#end")) '(("a" . "b") ("b" . "#end"))))

(test-success "list->last-time-record"
  (equal? (list->last-time-record '()) #f)
  (equal? (list->last-time-record '("#end")) #f)
  (equal? (list->last-time-record '("a")) "a")
  (equal? (list->last-time-record '("a" "#end")) #f)
  (equal? (list->last-time-record '("a" "#end" "#end")) #f)
  (equal? (list->last-time-record '("a" "b")) "b")
  (equal? (list->last-time-record '("a" "b" "c")) "c")
  (equal? (list->last-time-record '("a" "b" "#end")) #f))

(with-root "short-note"
  (lambda ()
    (make-short-note "test" "foo")
    (let ((path (string-append chandai-root "/test/2009/07/23")))
      (test-success "make-short-note"
	(file-exists? path)
	(equal? (with-input-from-file path read-line)
		"2009/07/23 20:48:21+1000 foo")))

    (test-success "list-short-notes"
      (equal? (begin
		(list-short-notes "test" #f #f)
		(with-input-from-file (string-append chandai-root "/test/.recent") read))
	      (list "2009/07/23 20:48:21+1000 foo"))
      (equal? (begin
		(list-short-notes "test" "foo" #f)
		(with-input-from-file (string-append chandai-root "/test/.recent") read))
	      (list "2009/07/23 20:48:21+1000 foo"))
      (equal? (begin
		(list-short-notes "test" "fooz" #f)
		(with-input-from-file (string-append chandai-root "/test/.recent") read))
	      '())
      (equal? (begin
		(list-short-notes "test" #f "*23")
		(with-input-from-file (string-append chandai-root "/test/.recent") read))
	      (list "2009/07/23 20:48:21+1000 foo")))))

(test-end)
