(load "../chandai")
(load "../native")
(load "../short-note")
(load "../long-note")
(load "../srfi-19")
(load "../time")
(load "../misc")
(load "../main")

(##include "../chandai#.scm")
(##include "../time#.scm")

(set! chandai-root (path-expand "dummy")) ; you would not want tests to ruin you real data

(define test-time "2009/07/23 20:48:21+1000")
(set! make-current-time (lambda () (note-string->time test-time)))

(define (with-root root func)
  (define (remove path)
;;    (define (delete-directory foo)
;;      (print "Deleting directory " foo "\n"))
;;    (define (delete-file foo)
;;      (print "Deleting file " foo "\n"))
    (and (file-exists? path)
	 (case (file-type path)
	   ((directory)
	    (begin
	      (for-each (lambda (p)
			  (remove (string-append path "/" p)))
			(directory-files (list (string->keyword "path") path
					       (string->keyword "ignore-hidden") 'dot-and-dot-dot)))
	      (delete-directory path)))
	   (else
	    (delete-file path)))))

  (let ((old-root chandai-root)
	(new-root (path-expand root)))
    (if (file-exists? new-root)
	(raise (string-append "Root " new-root " already exists."))
	(begin
	  (set! chandai-root new-root)
	  (let ((result (func)))
	    (set! chandai-root old-root)
	    (and result
		 (remove new-root))))))) ; don't remove if result is not #f
