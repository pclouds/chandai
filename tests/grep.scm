(load "../native")
(load "../sort")
(load "../grep")

(include "../grep#.scm")

(test-begin "grep")

(let ((fn (grep-substr-fn "abc")))
  (test-success "grep-substr-fn"
    (not (fn "a"))
    (fn "abc")
    (fn "abc")
    (fn "aabc")
    (fn "abcc")))

(test-success "grep-fnmatch-fn"
  ((grep-fnmatch-fn "a*") "a")
  ((grep-fnmatch-fn "a*") "abc")
  (not ((grep-fnmatch-fn "a*") ""))

  ((grep-fnmatch-fn "a?") "ab")
  ((grep-fnmatch-fn "a?") "ab")
  (not ((grep-fnmatch-fn "a?") "abc"))
  (not ((grep-fnmatch-fn "a?") "a"))

  ((grep-fnmatch-fn "[abc]") "a")
  ((grep-fnmatch-fn "[abc]") "b")
  ((grep-fnmatch-fn "[abc]") "c")
  (not ((grep-fnmatch-fn "[abc]") "d")))

;(test-error ((grep-fnmatch-fn "[abc") "d"))
; test for invalid input

(test-success "grep-re-fn"
  ((grep-re-fn "abc") "abc")
  ((grep-re-fn "abc") "aabcc")
  ((grep-re-fn "aa*") "abc")
  ((grep-re-fn "aa*") "aaabc")
  (not ((grep-re-fn "aa*") "bc")))
; test for invalid regexp
; test for invalid input

(let ((fn (make-grep-and-fn (list (grep-substr-fn "abc")
				  (grep-substr-fn "def")))))
  (test-success "grep and combination"
    (fn "abcdef")
    (not (fn "abc"))
    (not (fn "def"))))

(let ((fn (make-grep-or-fn (list (grep-substr-fn "abc")
				 (grep-substr-fn "def")))))
  (test-success "grep or combination"
    (fn "abcdef")
    (fn "abc")
    (fn "def")
    (not (fn "ab"))))

(let ((fn (make-grep-not-fn (grep-substr-fn "abc"))))
  (test-success "grep not combination"
    (not (fn "abc"))
    (fn "a")))

(let ((fn (make-grep-sexp-fn grep-substr-fn (list 'and "a" (list "b" "c")))))
  (test-success "grep sexp combination"
    (fn "abc")
    (fn "ab")
    (fn "ac")
    (not (fn "a"))
    (not (fn "bc"))
    (not (fn "def"))))

(test-success "grep-list"
 (equal? (grep-list (grep-substr-fn "abc")
		    (list "abc" "abc" "def" "ab"))
	 (list "abc" "abc"))
 (equal? (grep-list (grep-substr-fn "def")
		    (list "abc" "abc" "def" "ab"))
	 (list "def"))
 (equal? (grep-list (grep-substr-fn "abcd")
		    (list "abc" "abc" "def" "ab"))
	 '()))

(test-success "grep-port"
  (equal? (call-with-input-file "grep-1.txt"
	    (lambda (port)
	      (grep-port (grep-substr-fn "abc") port)))
	  (list "abc" "abcdef"))
  (equal? (call-with-input-file "grep-1.txt"
	    (lambda (port)
	      (grep-port (grep-substr-fn "ghi") port)))
	  '()))

(test-success "grep-files"
  (equal? (grep-files (grep-substr-fn "abc")
		      (list "grep-1.txt" "grep-2.txt"))
	  (list "abc" "abcdef" "abcdef" "abc"))
  (equal? (grep-files (grep-substr-fn "abc") '())
	  '()))

(test-success "grep-files-2"
  (equal? (grep-files-2 (grep-substr-fn "abc")
			(list "grep-1.txt" "grep-2.txt"))
	  '(("grep-1.txt" ("abc" "abcdef"))
	    ("grep-2.txt" ("abcdef" "abc")))))

(test-end)
