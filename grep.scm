(##namespace ("chandai#grep#"))
(##include "~~lib/gambit#.scm")

(##include "native#.scm")
(##include "sort#.scm")

;; Traverse through a list and apply match? on it
(define (grep-list match? list)
  (define (grep-by-fn match? get-line)
    (let ((line (get-line)))
      (if (string? line)
	  (if (match? line)
	      (cons line (grep-by-fn match? get-line))
	      (grep-by-fn match? get-line))
	  '())))

  (grep-by-fn match?
	      (lambda ()
		(if (pair? list)
		    (let ((this-line (car list)))
		      (set! list (cdr list))
		      this-line)
		    list))))

(define (grep-port match? port)
  (let ((line (read-line port)))
    (if (string? line)
	(if (match? line)
	    (cons line (grep-port match? port))
	    (grep-port match? port))
	'())))

(define (grep-files match? list)
  (cond
   ((pair? list)
    (call-with-input-file (car list)
      (lambda (port)
	(let ((result (grep-port match? port)))
	  (cond
	   ((pair? result)
	    (append result (grep-files match? (cdr list))))
	   (null?
	    (grep-files match? (cdr list)))
	   (else
	    (cons (grep-files match? (cdr list)))))))))
   ((null? list)
    '())
   (else
    (call-with-input-file list
      (lambda (port)
	(grep-port match? port))))))

;; a list of (filename . (line line line))
(define (grep-files-2 match? lst)
  (map (lambda (x)
	 (list x (call-with-input-file x
		   (lambda (port)
		     (grep-port match? port)))))
       lst))

;; Grep routines

(define (grep-substr-fn substr)
  (lambda (line)
    (c-strstr line substr)))

(define (grep-re-fn pattern)
  (let ((re (c-regcomp pattern)))
    (lambda (line)
      (c-regexec re line))))

(define (grep-fnmatch-fn pattern)
  (lambda (line)
    (c-fnmatch pattern line)))

;; Grep combination

(define (make-grep-and-fn lst)
  (lambda (line)
    (call/cc
     (lambda (exit)
       (let loop ((p lst))
	 (cond
	  ((pair? p)
	   (if ((car p) line)
	       (loop (cdr p))
	       (exit #f)))
	  ((null? p)
	   #t)
	  (else
	   (p line))))))))

(define (make-grep-or-fn lst)
  (lambda (line)
    (call/cc
     (lambda (exit)
       (let loop ((p lst))
	 (cond
	  ((pair? p)
	   (if ((car p) line)
	       (exit #t)
	       (loop (cdr p))))
	  ((null? p)
	   #f)
	  (else
	   (if (p line)
	       (exit #t)
	       #f))))))))

(define (make-grep-not-fn fn1)
  (lambda (line)
    (not (fn1 line))))

(define (make-grep-sexp-fn grep-fn lst)
  (cond
   ((pair? lst)
    (cond
     ((symbol? (car lst))
      (cond
       ((eq? (car lst) 'and)
	(make-grep-and-fn (map (lambda (x)
				 (make-grep-sexp-fn grep-fn x))
			       (cdr lst))))
       ((eq? (car lst) 'or)
	(make-grep-or-fn (map (lambda (x)
				(make-grep-sexp-fn grep-fn x))
			      (cdr lst))))
       ((eq? (car lst) 'not)
	(make-grep-not-fn (make-grep-sexp-fn grep-fn (cadr lst))))
       ((eq? (car lst) 'substr-fn)
	(make-grep-sexp-fn grep-substr-fn (cdr lst)))
       ((eq? (car lst) 're-fn)
	(make-grep-sexp-fn grep-re-fn (cdr list)))
       ((eq? (car lst) 'fnmatch-fn)
	(make-grep-sexp-fn grep-fnmatch-fn (cdr list)))
       (else
	(make-grep-or-fn (map (lambda (x)
				(make-grep-sexp-fn grep-fn x))
			      (cdr lst))))))
     (else
      (make-grep-or-fn (map (lambda (x)
			      (make-grep-sexp-fn grep-fn x))
			    lst)))))
   ((null? lst)
    '())
   ((string? lst)
    (grep-fn lst))
   ((procedure? lst)
    lst)
   (else
    (lambda (x) #t))))

;; File selection

(define (ls-files pattern root)
  ;; It's quite inefficient, need to check for directory
  (define (ls-files-1 root)
    (define (with-open-directory path cont)
      (let* ((dir (open-directory path))
	     (result (cont dir)))
	(close-input-port dir)
	result))

    (with-open-directory root
			 (lambda (dir)
			   (let next ((p (read dir)))
			     (if (string? p)
				 (let ((pd (string-append root "/" p)))
				   ;; TODO: bubble sort?
				   (if (eq? (file-type pd) 'directory)
				       (append (ls-files-1 pd)
					       (next (read dir)))
				       (cons pd (next (read dir)))))
				 '())))))
    (sort (cond
	   ((string? pattern)
	    (let ((root-length (+ (string-length root) 1))) ; there is a trailing slash, not in root
	      (grep-list (lambda (x)
			   (c-fnmatch pattern (substring x root-length (string-length x))))
			 (ls-files-1 root))))
	   ((pair? pattern)
	    (let ((root-length (+ (string-length root) 1))) ; there is a trailing slash, not in root
	      (grep-list (make-grep-sexp-fn (lambda (pattern)
					      (lambda (line)
						(c-fnmatch pattern
							   (substring line root-length (string-length line)))))
					    pattern)
			 (ls-files-1 root))))
	   (else
	      (ls-files-1 root)))
	  string<=?))
