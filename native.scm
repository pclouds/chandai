(##namespace ("chandai#native#"))
(##include "~~/lib/gambit#.scm")

(c-declare #<<c-declare-end
#include <sys/types.h>
#include <fnmatch.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
c-declare-end
)

(define c-strstr
  (c-lambda (nonnull-char-string nonnull-char-string) scheme-object
#<<c-lambda-end
  const char *result = strstr(___arg1, ___arg2);
  ___result = result ? ___FIX(result - ___arg1) : ___FAL;
c-lambda-end
))

(define c-fnmatch
  (c-lambda (nonnull-char-string nonnull-char-string) bool
#<<c-lambda-end
	___result = fnmatch(___arg1, ___arg2, 0) == 0;
c-lambda-end
))

(define c-regcomp
  (c-lambda (nonnull-char-string) (pointer "regex_t") #<<c-lambda-end
	regex_t *t;

	t = (regex_t*)malloc(sizeof(regex_t));
	regcomp(t, ___arg1, 0);
	___result_voidstar = t;
c-lambda-end
))

(define c-regexec
  (c-lambda ((pointer "regex_t") nonnull-char-string) bool
#<<c-lambda-end
	___result = regexec(___arg1, ___arg2, 0, NULL, 0) == 0;
c-lambda-end
))

(define c-regfree
  (c-lambda ((pointer "regex_t")) void
#<<c-lambda-end
	regfree(___arg1);
	free(___arg1);
c-lambda-end
))

(define c-timezone
  (c-lambda () int
#<<c-lambda-end
	___result = -timezone;
c-lambda-end
))

(define c-println
  (c-lambda (nonnull-char-string) int
#<<c-lambda-end
	int i, len, newlen;
	len = newlen = strlen(___arg1);
	if (isatty(1)) {
		for (i = 0;i < len;i ++) {
			if (___arg1[i] == '#' || ___arg1[i] == '@')
				newlen += 10;
		}
	}
	if (len == newlen)
		___result = printf("%s\n", ___arg1);
	else {
		char *newbuf, *p;
		int code, l;

		newbuf = p = malloc(newlen+1);
		for (i = 0;i < len;i ++) {
			if (___arg1[i] != '#' && ___arg1[i] != '@') {
				*p++ = ___arg1[i];
				continue;
			}
			if (___arg1[i] == '#' && !strncmp(___arg1+i+1, "todo", 4)) {
				code = 31;
				l = 5;
			}
			else if (!strncmp(___arg1+i+1, "clock", 5)) {
				code = 32;
				l = 6;
			}
			else if (!strncmp(___arg1+i+1, "end", 3)) {
				code = 33;
				l = 4;
			}
			else if (___arg1[i] == '#') {
				code = 1;
				l = 1;
				while (l+i < len &&
				       ((___arg1[l+i] >= 'a' && ___arg1[l+i] <= 'z') ||
				        (___arg1[l+i] >= 'A' && ___arg1[l+i] <= 'Z') ||
				        (___arg1[l+i] >= '0' && ___arg1[l+i] <= '9') ||
					___arg1[l+i] == '-' || ___arg1[l+i] == '_'))
					l++;
			}
			else if (___arg1[i] == '@') {
				code = 4;
				l = 1;
				while (l+i < len &&
				       ((___arg1[l+i] >= 'a' && ___arg1[l+i] <= 'z') ||
				        (___arg1[l+i] >= 'A' && ___arg1[l+i] <= 'Z') ||
				        (___arg1[l+i] >= '0' && ___arg1[l+i] <= '9') ||
					___arg1[l+i] == '-' || ___arg1[l+i] == '_'))
					l++;
				if (___arg1[l+i] == '[')
					while (l+i < len && ___arg1[l+i] != ']')
						l++;
			}
			else
				code = 0;
			if (code) {
				p += sprintf(p, "\033[%dm%.*s\033[m", code, l, ___arg1+i);
				i += l-1;
			}
		}
		*p = '\0';
		___result = printf("%s\n", newbuf);
		free(newbuf);
	}
	fflush(stdout);
c-lambda-end
))


(define c-sanitize-note-for-linking
  (c-lambda (nonnull-char-string) nonnull-char-string
#<<c-lambda-end
	int i, len = strlen(___arg1);
	___result = malloc(len+1);
	memcpy(___result, ___arg1, len+1);
	for (i = 0;i < len;i++) {
		if (___result[i] == '@' || ___result[i] == '#')
			___result[i] = '&';
		if (___result[i] == '[')
			___result[i] = '{';
		if (___result[i] == ']')
			___result[i] = '}';
	}
c-lambda-end
))

(c-initialize "tzset();")
