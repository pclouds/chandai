(##namespace ("chandai#git#"))
(##include "~~lib/gambit#.scm")

(##include "chandai#.scm")

(define (git-init)
  (let* ((root chandai-root)
	 (git-dir (string-append root "/.git")))
    (or (and (file-exists? root) (eq? (file-type root) 'directory))
	(create-directory root))
    (or (and (file-exists? git-dir) (eq? (file-type git-dir) 'directory))
	(shell-command (string-append "cd " root " && git init")))))
