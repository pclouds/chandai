(##namespace ("chandai#long-note#"))
(##include "~~lib/gambit#.scm")

(##include "chandai#.scm")
(##include "git#.scm")
(##include "grep#.scm")
(##include "misc#.scm")
(##include "native#.scm")
(##include "short-note#.scm")
(##include "time#.scm")

(define (make-long-note prefix note)
  (define (prepare-path path)
    (or (file-exists? path)
	(create-directory-recursively path)))

  (git-init)
  (let* ((time (make-current-time))
	 (path (time->long-note-path prefix time)))
    (prepare-path (path-expand path))
    (with-output-to-file (string-append path "/chandai")
      (lambda ()
	(write
	 (list (cons 'subject note)
	       (cons 'hidden-subject "")
	       (cons 'created (time->note-string time)))))))
  (invalidate-recent-cache prefix))

;; Very simple, hidden-subject not supported yet
(define (long-note->short-note path)
  (let ((note (with-input-from-file path read)))
    (string-append (cdr (assoc 'created note)) " " (cdr (assoc 'subject note)))))

(define (grep-long-notes prefix patterns pathspecs)
  ;; TODO: unify back to grep.scm's grep-files
  (define (grep-files match? lst)
    (cond
     ((pair? lst)
      (let ((long-note (long-note->short-note (car lst))))
	(if (match? long-note)
	  (cons long-note (grep-files match? (cdr lst)))
	  (grep-files match? (cdr lst)))))
     ((null? lst)
      '())
     (else
      (if (match? (long-note->short-note lst))
	  lst
	  #f))))

  (let* ((full-prefix (string-append chandai-root "/" prefix))
	 (files (ls-files "*/chandai" full-prefix)))
    (grep-files (make-grep-sexp-fn grep-re-fn patterns) files)))

(define (list-long-notes prefix patterns pathspecs)
  (let ((result (grep-long-notes prefix patterns pathspecs)))
    (cache-search-results prefix result)
    (map (lambda (x) (c-println x)) result)))
